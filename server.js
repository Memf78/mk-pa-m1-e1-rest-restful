var express = require('express') ;
var app = express();
var bodyParser = require('body-parser');
var requestJson = require('request-json');

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechumemf/collections/";
var mlabAPIKey = "apiKey=82c8W8wP4IFmYvNIAsiHHceCu2RrD2BJ";

app.use(bodyParser.json());
var port = process.env.PORT || 3000;

app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1',
    function(req, res) {
        console.log("GET /apitechu/v1");
        res.send(
          {
            "msg":"Bienvenido a mi API"
          }
        )
    }
);

app.get('/apitechu/v1/users',
   function(req, res)
   {
     console.log("GET /apitechu/v1/users");
     res.sendFile('usuarios.json', {root:__dirname});
   }
);

app.get('/apitechu/v2/users',
function(req, res)
{
  console.log("GET /apitechu/v2/users");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("user?" + mlabAPIKey,
  function(err, resMLab, body) {
    var response = !err ? body : {
      "msg":"Erroe obteniendo usuarios"
    }
    res.send(response);
  }
)
}
);

app.get('/apitechu/v2/users/:id',
function(req, res)
{
  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  console.log("GET /apitechu/v2/users/:id");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("user?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);


app.post('/apitechu/v2/login',
function(req, res)
{
// Tomo datos del body
 var newUser = {
    "email" : req.body.email,
    "password" : req.body.password
 };

 var query = 'q={"email" : "' + newUser.email + '"}';

 console.log("mail" + newUser.email);
 console.log("pass" + newUser.password);
 console.log("GET /apitechu/v2/login");
 // Creo cliente: conexion a MLAB para accesos a db
 httpClient = requestJson.createClient(baseMlabURL);
 console.log("cliente creado");
 // Inicializo repuesta KO
 response = {
                "msg" : "loggin incorrecto"
             };
 // Prefiero acceder a db por mail y validar el pass por software
 // para diferenciar errores
 httpClient.get("user?" + query + "&" + mlabAPIKey,
 function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
      } else {
        console.log("mail" + newUser.email);
        console.log("pass" + newUser.password);
          if (body.length > 0) {
//  -------------------------
             console.log("body[0].password" + body[0].password);
             console.log("pass" + newUser.password);
             // valido el pass por software
             if (body[0].password == newUser.password)
               {
                 var queryPut = 'q={"id" : ' + body[0].id + '}';
                 var putBody = '{"$set":{"logged":true}}';
                 console.log("queryput" + queryPut );
                 // alta de campo para login ok
                 httpClient.put("user?" + queryPut + "&" + mlabAPIKey,
                      JSON.parse(putBody)
                      );
                      response = {
                                   "msg" : "loggin correcto"
                                 };
               }
             }
//-------------------------------------------
           }
        res.send(response);
      }
    )
    }
);


//------------------------

app.post('/apitechu/v1/users',
   function(req, res)
   {
     console.log("POST /apitechu/v1/users");

     var newUser = {
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "country" : req.body.country
     };

     var users = require('./usuarios.json');
     users.push(newUser);
     writeUserDataToFile(users);
//     console.log("Usuario añadido con exito");
     res.send(
       {
         "msg":"Operación completda"
       }
     );

     console.log("first_name es" + req.body.first_name);
     console.log("last_name es" + req.body.last_name);
     console.log("country es" + req.body.country);

   }
);

app.delete ('/apitechu/v1/users/:id',
   function(req, res)
   {
     console.log("DELETE /apitechu/v1/users:id");
     var users = require('./usuarios.json');
     users.splice(req.params.id - 1, 1);
     writeUserDataToFile(users);
     res.send(
       {
         "msg":"Operación completda"
       }
     );
   }
);

function writeUserDataToFile(data)
{
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json",
   jsonUserData,
   "utf8",
   function(err) {
     if (err) {
          console.log(err);
        } else {
            console.log("Usuario persistido");
        }
     }
 )
}

//localhost:3000/apitechu/v1/monstruo/param1/param2?query_param1=valor1&query_param2=valor2
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res) {
    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);

app.post('/apitechu/v1/login',
   function(req, res)
   {
     console.log("POST /apitechu/v1/login");

     var newUser = {
        "email" : req.body.email,
        "password" : req.body.password
     };

     var users = require('./usulogin.json');
//     users.push(newUser);
     var encontrado = false

     console.log("mail" + newUser.email);
     console.log("encontrado" + encontrado);
     console.log("pass" + newUser.password);

     for (user of users) {
          if ((user.email == newUser.email)&&(user.password == newUser.password))
          {
                 console.log("usuario dentro" + user.email);
                  console.log("password dentro" + user.password);
                  console.log("name dentro" + user.first_name);
                  encontrado = true;
                  user.logged = true;
                  res.send(
                    {
                      "msg": "login correcto",
                      "id" :  user.id
                    }
                  );
                  break;

          }
     }
     writeUserDataToFile2(users);
     if (encontrado == false)
     res.send(
       {
         "msg":"login incorrecto"
       }
     );

     console.log("encontrado" + encontrado);
     console.log("mail" + req.body.email);
     console.log("last_name es" + req.body.password);

   }
);

app.post('/apitechu/v1/logout',
   function(req, res)
   {
     console.log("POST /apitechu/v1/logout");
     var newUser = {
        "id" : req.body.id
     };

     var users = require('./usulogin.json');
     var encontrado = false

     console.log("newid " + newUser.id);
     console.log("encontrado" + encontrado);

     for (user of users) {
         console.log("id dentro" + user.id);
          if ((user.id == newUser.id)&&(user.logged==true))
                {
                  console.log("id dentro" + user.id);
                  console.log("name dentro" + user.first_name);
                  encontrado = true;
                  delete user.logged;
                  res.send(
                    {
                      "msg": "logout correcto",
                      "id" :  user.id
                    }
                  );
                  break;
          }
     }

     writeUserDataToFile2(users);

       if (encontrado == false)
       res.send(
         {
           "msg":"logout incorrecto"
         }
       );
   }
);

function writeUserDataToFile2(data)
{
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usulogin.json",
   jsonUserData,
   "utf8",
   function(err) {
     if (err) {
          console.log(err);
        } else {
            console.log("Usuario persistido");
        }
     }
 )
}

// --------------------------------------------- xxxxxxxxx

app.get('/apitechu/v2/users/:id/account',
function(req, res)
{
  var user_id = req.params.id;
  var query = 'q={"user_id" : ' + user_id + '}';

  console.log("GET /apitechu/v2/users/:id/account");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("account?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "cuenta no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);
